﻿using DelegatesAndEvents.Events;
using DelegatesAndEvents.Extensions;
using DelegatesAndEvents.IO;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DelegatesAndEvents.App
{
    class Program
    {
        static async Task Main()
        {
            try
            {
                var testItems = GetTestDataItems();
                var maxItem = testItems.GetMax(i => i.Value);
                Console.WriteLine($"Item with maximum value: {maxItem}");

                Console.WriteLine("Enter directory path to search files:");
                string directoryPath = Console.ReadLine();
                var directoryFilesSearcher = new DirectoryFilesSearcher(directoryPath);
                directoryFilesSearcher.FileFound += DirectoryFilesSearcher_FileFound;
                CancellationTokenSource cts = new CancellationTokenSource();
                CancellationToken token = cts.Token;

                var searchTask = directoryFilesSearcher.StartSearchAsync(token);
                await searchTask;

                if (searchTask.IsCompleted)
                {
                    Console.WriteLine("Files search has been finished.");
                }
                else
                {
                    Console.WriteLine("Files search started... Press any key to cancel:");
                    Console.ReadKey();
                    Console.WriteLine();
                    cts.Cancel();

                    Console.WriteLine(searchTask.IsCanceled ?
                        "Files search has been canceled." :
                        "Files search has been finished before cancellation.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception occured: {ex}");
            }
        }

        private static void DirectoryFilesSearcher_FileFound(object sender, FileEventArgs e)
        {
            var parent = sender as DirectoryFilesSearcher;
            Console.WriteLine(@$"Found file ""{e.FilePath}"" in directory ""{parent.DirectoryPath}""");
        }

        private static List<TestData> GetTestDataItems()
        {
            return new List<TestData>()
            {
                new TestData(Guid.NewGuid(), 5.9f),
                new TestData(Guid.NewGuid(), 3.3f),
                new TestData(Guid.NewGuid(), 3.3f),
                new TestData(Guid.NewGuid(), 8.1f),
                new TestData(Guid.NewGuid(), 1f)
            };
        }
    }
}
