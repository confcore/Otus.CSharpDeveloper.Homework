﻿using System;

namespace DelegatesAndEvents.App
{
    public class TestData
    {
        public TestData(Guid id, float value)
        {
            Id = id;
            Value = value;
        }

        public float Value { get; }

        public Guid Id { get; }

        public override string ToString()
        {
            return $"{Id} - {Value}";
        }
    }
}
