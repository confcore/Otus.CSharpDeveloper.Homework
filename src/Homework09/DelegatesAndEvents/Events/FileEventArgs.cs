﻿using System;

namespace DelegatesAndEvents.Events
{
    /// <summary>
    /// Аргументы события файлового поиска.
    /// </summary>
    public class FileEventArgs : EventArgs
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="filePath">Путь к файлу.</param>
        public FileEventArgs(string filePath)
        {
            if (filePath == null)
            {
                throw new ArgumentNullException(nameof(filePath));
            }

            FilePath = filePath;
        }

        /// <summary>
        /// Путь к файлу.
        /// </summary>
        public string FilePath { get; }
    }
}
