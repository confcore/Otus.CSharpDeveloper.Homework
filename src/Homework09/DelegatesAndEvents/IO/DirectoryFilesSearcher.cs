﻿using DelegatesAndEvents.Events;
using DelegatesAndEvents.Extensions;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace DelegatesAndEvents.IO
{
    /// <summary>
    /// Класс, обходящий каталог файлов и выдающий событие при нахождении каждого файла.
    /// </summary>
    public class DirectoryFilesSearcher
    {
        private static readonly Regex windowsDirectoryPathRegex = new Regex(@"^[a-zA-Z]:\\(((?![<>:"" /\\|? *]).)+((?<![ .])\\)?)*$", RegexOptions.Compiled);
        private static readonly Regex linuxDirectoryPathRegex = new Regex(@"^/|(/[a-zA-Z0-9_-]+)+$", RegexOptions.Compiled);

        /// <summary>
        /// Событие, срабатывающее при нахождении каждого файла.
        /// </summary>
        public event EventHandler<FileEventArgs> FileFound;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="path">Путь до каталога.</param>
        public DirectoryFilesSearcher(string path)
        {
            if (!windowsDirectoryPathRegex.IsMatch(path) && !linuxDirectoryPathRegex.IsMatch(path))
            {
                throw new ArgumentException("Path must be a valid directory path.");
            }

            DirectoryPath = path;
        }

        /// <summary>
        /// Путь до каталога.
        /// </summary>
        public string DirectoryPath { get; }

        /// <summary>
        /// Обойти каталог файлов.
        /// </summary>
        public void StartSearch()
        {
            foreach (var file in Directory.EnumerateFiles(DirectoryPath))
            {
                var fileEventArgs = new FileEventArgs(file);
                OnFileFound(fileEventArgs);
            }
        }

        /// <summary>
        /// Обойти каталог файлов асинхронно.
        /// </summary>
        /// <param name="token">Токен отмены операции.</param>
        public async Task StartSearchAsync(CancellationToken token)
        {
            if (token.IsCancellationRequested)
            {
                return;
            }

            await Task.Run(() =>
            {
                foreach (var file in Directory.EnumerateFiles(DirectoryPath))
                {
                    if (token.IsCancellationRequested)
                    {
                        return;
                    }

                    var fileEventArgs = new FileEventArgs(file);
                    OnFileFound(fileEventArgs);
                }
            });
        }

        protected virtual void OnFileFound(FileEventArgs e)
        {
            e.Raise(this, ref FileFound);
        }
    }
}
