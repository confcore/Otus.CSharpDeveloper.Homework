﻿using System;
using System.Collections.Generic;

namespace DelegatesAndEvents.Extensions
{
    /// <summary>
    /// Расширения для <see cref="IEnumerable{T}"/>.
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Обобщённая функция расширения, находящая и возвращающая максимальный элемент коллекции.
        /// </summary>
        /// <typeparam name="T">Тип элемента коллекции.</typeparam>
        /// <param name="items">Коллекция.</param>
        /// <param name="getValue">Делегат, преобразующий входной тип в число для возможности поиска максимального значения.</param>
        /// <returns>Максимальный элемент коллекции.</returns>
        public static T GetMax<T>(this IEnumerable<T> items, Func<T, float> getValue)
            where T : class
        {
            if (items == null)
            {
                throw new ArgumentNullException(nameof(items));
            }

            if (getValue == null)
            {
                throw new ArgumentNullException(nameof(getValue));
            }

            var maxValue = float.MinValue;
            T maxItem = null;
            foreach (var item in items)
            {
                var itemValue = getValue(item);
                if (itemValue > maxValue)
                {
                    maxValue = itemValue;
                    maxItem = item;
                }
            }

            return maxItem;
        }
    }
}
