﻿using System;
using System.Threading;

namespace DelegatesAndEvents.Extensions
{
    /// <summary>
    /// Расширения для <see cref="EventArgs"/>.
    /// </summary>
    public static class EventArgsExtensions
    {
        /// <summary>
        /// Обобщённая функция расширения для потокобезопасного вызова события.
        /// </summary>
        /// <typeparam name="TEventArgs">Тип аргументов события.</typeparam>
        /// <param name="e">Аргументы события.</param>
        /// <param name="sender">Родитель.</param>
        /// <param name="eventHandler">Обработчик события.</param>
        public static void Raise<TEventArgs>(this TEventArgs e, object sender, ref EventHandler<TEventArgs> eventHandler)
        {
            Volatile.Read(ref eventHandler)?.Invoke(sender, e);
        }
    }
}
