﻿using System.Collections;
using System.Collections.Generic;

namespace HtmlRegexParser.Tests.ClassData
{
    /// <summary>
    /// Test class data for <see cref="HtmlUrlParserTests"/>.
    /// </summary>
    internal class HtmlUrlParserTestData : IEnumerable<object[]>
    {
        private readonly List<object[]> data = new List<object[]>
        {
            new object[]
            {
                @"}(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');",
                new List<string>()
                {
                    @"https://connect.facebook.net/en_US/fbevents.js"
                }
            },
            new object[]
            {
                @"<link href=""https://otus.ru/static/img/favicons/android-chrome-537x240.jpg"" rel=""image_src""/> <meta property=""vk:image"" content=""https://otus.ru/static/img/favicons/android-chrome-537x240.jpg"">",
                new List<string>()
                {
                    @"https://otus.ru/static/img/favicons/android-chrome-537x240.jpg",
                    @"https://otus.ru/static/img/favicons/android-chrome-537x240.jpg"
                }
            },
            new object[]
            {
                @"<a href=""https://teleg.run/Otushelp_bot"" rel=""nofollow noreferrer noopener",
                new List<string>()
                {
                    @"https://teleg.run/Otushelp_bot"
                }
            },
            new object[]
            {
                @" })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KQQBQLZ');</script>",
                new List<string>()
                {
                    @"www.google-analytics.com/analytics.js",
                    @"https://www.googletagmanager.com/gtm.js?id="
                }
            }
        };

        public IEnumerator<object[]> GetEnumerator() => data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
