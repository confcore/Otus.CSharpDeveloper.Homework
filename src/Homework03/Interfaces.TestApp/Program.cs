﻿using Interfaces.Core.Task1.Serialization;
using Interfaces.Core.Task3.Dtos;
using Interfaces.Core.Task3.Repositories;
using System;

namespace Interfaces.TestApp
{
    class Program
    {
        static void Main()
        {
            var repository = new Repository<Account>(new OtusJsonSerializer());
            repository.Add(new Account()
            {
                LastName = "Jones",
                FirstName = "Jon",
                BirthDate = new DateTime(1991, 1, 1)
            });

            repository.Add(new Account()
            {
                LastName = "Peterson",
                FirstName = "Peter",
                BirthDate = new DateTime(1992, 2, 2)
            });

            Console.WriteLine("All accounts in DB:");
            var accounts = repository.GetAll();
            foreach (var account in accounts)
            {
                Console.WriteLine(account);
            }

            var foundAccount = repository.GetOne(i => i.FirstName == "Peter");
            Console.WriteLine($"Found account with FirstName == Peter: {foundAccount}");
        }
    }
}
