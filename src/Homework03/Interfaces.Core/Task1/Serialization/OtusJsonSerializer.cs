﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace Interfaces.Core.Task1.Serialization
{
    public class OtusJsonSerializer : ISerializer
    {
        public T Deserialize<T>(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            using StreamReader reader = new StreamReader(stream);
            using JsonTextReader jsonReader = new JsonTextReader(reader);
            JsonSerializer serializer = new JsonSerializer();
            return serializer.Deserialize<T>(jsonReader);
        }

        public string Serialize<T>(T item)
        {
            return JsonConvert.SerializeObject(item);
        }
    }
}
