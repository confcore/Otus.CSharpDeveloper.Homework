﻿using Interfaces.Core.Task3.Dtos;

namespace Interfaces.Core.Task3.Services
{
    public interface IAccountService
    {
        void AddAccount(Account account);
    }
}
