﻿using System;

namespace Interfaces.Core.Task3.Dtos
{
    public class Account
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime BirthDate { get; set; }

        public override string ToString()
        {
            return $"{LastName} {FirstName} {BirthDate}";
        }
    }
}
