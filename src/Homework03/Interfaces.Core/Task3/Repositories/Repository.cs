﻿using Interfaces.Core.Task1.IO;
using Interfaces.Core.Task1.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Interfaces.Core.Task3.Repositories
{
    public class Repository<T> : IRepository<T>
    {
        private const string DbName = "repository.db";
        private readonly List<T> items;
        private readonly ISerializer serializer;

        public Repository(ISerializer serializer)
        {
            this.serializer = serializer;
            items = GetItems();
        }

        public void Add(T item)
        {
            items.Add(item);
            SaveContext();
        }

        public IEnumerable<T> GetAll()
        {
            foreach (var item in items)
            {
                yield return item;
            }
        }

        public T GetOne(Func<T, bool> predicate)
        {
            return items.FirstOrDefault(predicate);
        }

        private void SaveContext()
        {
            var json = serializer.Serialize(items);
            File.WriteAllText(DbName, json);
        }

        private List<T> GetItems()
        {
            using var streamReader = new StreamReader(DbName);
            using var otusReader = new OtusStreamReader<T>(streamReader.BaseStream, serializer);
            return otusReader.ToList();
        }
    }
}
