﻿using System.Collections.Generic;

namespace Interfaces.Core.Task2.Algorithm
{
    public interface ISorter<T>
    {
        IEnumerable<T> Sort(IEnumerable<T> notSortedItems);
    }
}
