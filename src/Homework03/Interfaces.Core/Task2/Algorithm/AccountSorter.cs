﻿using Interfaces.Core.Task3.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Interfaces.Core.Task2.Algorithm
{
    public class AccountSorter : ISorter<Account>
    {
        public IEnumerable<Account> Sort(IEnumerable<Account> notSortedItems)
        {
            if (notSortedItems == null)
            {
                throw new ArgumentNullException(nameof(notSortedItems));
            }

            return notSortedItems
                .OrderBy(i => i.LastName)
                .ThenBy(i => i.FirstName)
                .ThenBy(i => i.BirthDate);
        }
    }
}
