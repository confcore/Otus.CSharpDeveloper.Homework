using Interfaces.Core.Task3.Dtos;
using Interfaces.Core.Task3.Helpers;
using Interfaces.Core.Task3.Repositories;
using Interfaces.Core.Task3.Services;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Interfaces.Tests
{
    public class AccountServiceTests
    {
        private readonly IAccountService accountService;

        public AccountServiceTests()
        {
            var accountRepository = new Mock<IRepository<Account>>();
            accountRepository.Setup(r => r.Add(It.IsAny<Account>())).Verifiable();

            //Fix current DateTime for correct unit-testing.
            var dateTimeProvider = new Mock<IDateTimeProvider>();
            dateTimeProvider.Setup(h => h.Now).Returns(new DateTime(2020, 4, 3));

            accountService = new AccountService(accountRepository.Object, dateTimeProvider.Object);
        }

        [Fact]
        public void ShouldThrowArgumentNullExceptionWhenAccountIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => accountService.AddAccount(null));
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void ShouldThrowArgumentExceptionWhenFirstNameNullOrWhiteSpace(string firstName)
        {
            var account = new Account()
            {
                FirstName = firstName,
                LastName = "LastName",
                BirthDate = DateTime.Now
            };

            var exception = Assert.Throws<ArgumentException>(() => accountService.AddAccount(account));
            Assert.Equal("FirstName cannot be null or whitespace.", exception.Message);
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void ShouldThrowArgumentExceptionWhenLastNameNullOrWhiteSpace(string lastName)
        {
            var account = new Account()
            {
                FirstName = "FirstName",
                LastName = lastName,
                BirthDate = DateTime.Now
            };

            var exception = Assert.Throws<ArgumentException>(() => accountService.AddAccount(account));
            Assert.Equal("LastName cannot be null or whitespace.", exception.Message);
        }

        [Theory]
        [MemberData(nameof(InvalidBirthDateData))]
        public void ShouldThrowWhenAgeLessThan18(DateTime birthDate)
        {
            var account = new Account()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                BirthDate = birthDate
            };

            var exception = Assert.Throws<ArgumentException>(() => accountService.AddAccount(account));
            Assert.Equal("Age cannot be less than 18 years.", exception.Message);
        }

        [Theory]
        [MemberData(nameof(ValidBirthDateData))]
        public void ShouldPassWhenAgeEqualOrGreaterThan18(DateTime birthDate)
        {
            var account = new Account()
            {
                FirstName = "FirstName",
                LastName = "LastName",
                BirthDate = birthDate
            };

            accountService.AddAccount(account);
        }

        public static IEnumerable<object[]> InvalidBirthDateData =>
            new List<object[]>
            {
                new object[] { new DateTime(2020, 4, 3) },
                new object[] { new DateTime(2015, 1, 1) },
                new object[] { new DateTime(2010, 12, 31) },
                new object[] { new DateTime(2013, 6, 6) },
            };

        public static IEnumerable<object[]> ValidBirthDateData =>
            new List<object[]>
            {
                new object[] { new DateTime(1988, 2, 29) },
                new object[] { new DateTime(1992, 1, 1) },
                new object[] { new DateTime(1995, 12, 31) },
                new object[] { new DateTime(2000, 6, 6) },
            };
    }
}
