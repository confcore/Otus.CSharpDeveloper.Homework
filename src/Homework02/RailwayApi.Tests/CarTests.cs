﻿using Xunit;

namespace RailwayApi.Tests
{
    /// <summary>
    /// Tests for <see cref="Car"/>.
    /// </summary>
    public class CarTests
    {
        [Fact]
        public void ShouldSetZeroCapacityByDefault()
        {
            var car = new Car();
            Assert.Equal<uint>(0, car.Capacity);
        }

        [Theory]
        [InlineData(5)]
        [InlineData(10)]
        public void ShouldSetCapacityWhenCreated(uint capacity)
        {
            var car = new Car(capacity);
            Assert.Equal(capacity, car.Capacity);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(5)]
        [InlineData(10)]
        public void ShouldEqualsWithSameCapacity(uint capacity)
        {
            var firstCar = new Car(capacity);
            var secondCard = new Car(capacity);
            Assert.Equal(firstCar, secondCard);
            Assert.True(firstCar.Equals(secondCard));
        }

        [Theory]
        [InlineData(0, 10)]
        [InlineData(10, 0)]
        [InlineData(1, 2)]
        [InlineData(2, 1)]
        public void ShouldNotEqualsWithDifferentCapacity(uint a, uint b)
        {
            var firstCar = new Car(a);
            var secondCard = new Car(b);
            Assert.NotEqual(firstCar, secondCard);
        }

        [Fact]
        public void ShouldBePassengerWithNonZeroCapacity()
        {
            var car = new Car(1);
            Assert.True(car.IsPassenger);
        }

        [Fact]
        public void ShouldBeNonPassengerWithZeroCapacity()
        {
            var car = new Car(0);
            Assert.False(car.IsPassenger);
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(5, 5)]
        [InlineData(10, 10)]
        public void ShouldEqualsByHashCode(uint a, uint b)
        {
            var firstCar = new Car(a);
            var secondCard = new Car(b);
            Assert.Equal(firstCar.GetHashCode(), secondCard.GetHashCode());
        }

        [Theory]
        [InlineData(0, 1)]
        [InlineData(1, 0)]
        [InlineData(10, 2)]
        [InlineData(5, 6)]
        public void ShouldDiffersByHashCode(uint a, uint b)
        {
            var firstCar = new Car(a);
            var secondCard = new Car(b);
            Assert.NotEqual(firstCar.GetHashCode(), secondCard.GetHashCode());
        }
    }
}
