﻿using System.Collections.Generic;
using System.Linq;
using RailwayApi.Extensions;
using Xunit;

namespace RailwayApi.Tests
{
    /// <summary>
    /// Tests for <see cref="TrainExtensions"/>.
    /// </summary>
    public class TrainExtensionsTests
    {
        [Fact]
        public void ShouldGetNonPassengerCars()
        {
            var train = new Train(new List<Car>()
            {
                new Car(0),
                new Car(1),
                new Car(0),
                new Car(10)
            }, 10);

            Assert.Equal(2, train.GetNonPassengerCars().Count());
        }
    }
}
