﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RailwayApi
{
    /// <summary>
    /// Train model.
    /// </summary>
    public class Train
    {
        public const uint MaximumCarsLimit = 20;
        private readonly List<Car> cars;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Train() : this(new List<Car>(), MaximumCarsLimit) { }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        /// <param name="cars">List of cars.</param>
        /// <param name="carsLimit">Maximum count of cars.</param>
        public Train(List<Car> cars, uint carsLimit)
        {
            if (cars == null)
            {
                throw new ArgumentNullException("cars");
            }

            if (cars.Count > MaximumCarsLimit)
            {
                throw new ArgumentException("Cars limit reached.");
            }

            if (cars.Count > carsLimit)
            {
                throw new ArgumentException("Cars count cannot be greater than limit.");
            }

            this.cars = cars;
            CarsLimit = carsLimit;
        }

        /// <summary>
        /// Train cars.
        /// </summary>
        public IEnumerable<Car> Cars
        {
            get
            {
                foreach (var car in cars)
                {
                    yield return car;
                }
            }
        }

        /// <summary>
        /// Maximum count of cars in train.
        /// </summary>
        public uint CarsLimit { get; }

        /// <summary>
        /// Train indexer.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <returns>Car by index.</returns>
        public Car this[int index] => cars[index];

        public static Train operator +(Train a, Train b)
        {
            uint carsCount = (uint)(a.Cars.Count() + b.Cars.Count());
            if (carsCount > MaximumCarsLimit)
            {
                throw new ArgumentOutOfRangeException("Cars limit reached.");
            }

            return new Train(a.Cars.Concat(b.Cars).ToList(), carsCount);
        }

        public static Train operator +(Train a, Car b)
        {
            uint carsCount = (uint)a.Cars.Count() + 1;
            if (carsCount > a.CarsLimit)
            {
                throw new ArgumentOutOfRangeException("Cars limit reached.");
            }

            return new Train(a.Cars.Concat(new List<Car>() { b }).ToList(), carsCount);
        }

        public static bool operator ==(Train a, Train b)
        {
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            if (ReferenceEquals(null, a))
            {
                return false;
            }

            return a.Equals(b);
        }

        public static bool operator !=(Train a, Train b) => !(a == b);

        /// <summary>
        /// Create a train from single car.
        /// </summary>
        /// <param name="car">Train car.</param>
        public static explicit operator Train(Car car)
        {
            return new Train(new List<Car>() { car }, 1);
        }

        public override bool Equals(object obj)
        {
            Train train = obj as Train;
            return !ReferenceEquals(null, train) 
                && CarsLimit == train.CarsLimit 
                && Cars.SequenceEqual(train.Cars);
        }

        /// <summary>
        /// GetHashCode implementation by Jon Skeet.
        /// </summary>
        /// <returns>HashCode.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                // Choose large primes to avoid hashing collisions.
                const int HashingBase = (int)2166136261;
                const int HashingMultiplier = 16777619;

                int hash = HashingBase;
                hash = (hash * HashingMultiplier) ^ (!ReferenceEquals(null, CarsLimit)
                    ? CarsLimit.GetHashCode() : 0);
                foreach (var car in cars)
                {
                    hash = (hash * HashingMultiplier) ^ (!ReferenceEquals(null, car)
                    ? car.GetHashCode() : 0);
                }
                return hash;
            }
        }

        public override string ToString()
        {
            return $"Cars count = {cars.Count} Cars limit = {CarsLimit}";
        }
    }
}
