﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using Reflection.Core;
using Reflection.Core.Serialization;
using System.Text.Json;

namespace Reflection.Benchmarks
{
    [SimpleJob(RuntimeMoniker.NetCoreApp31)]
    public class ReflectionVsJsonBenchmark
    {
        private TestData testData;
        private string testDataCsv;
        private string testDataJson;

        [Params(100, 100000)]
        public int N;

        [GlobalSetup]
        public void Setup()
        {
            testData = new TestData
            {
                PublicProperty1 = "test",
                PublicProperty2 = 123,
                PublicProperty3 = false
            };

            testDataCsv = testData.SerializeToCsv();
            testDataJson = JsonSerializer.Serialize(testData);
        }

        [Benchmark]
        public void Reflection_Serialization()
        {
            for (int i = 0; i < N; i++)
            {
                testData.SerializeToCsv();
            }
        }

        [Benchmark]
        public void Json_Serialization()
        {
            for (int i = 0; i < N; i++)
            {
                JsonSerializer.Serialize(testData);
            }
        }

        [Benchmark]
        public void Reflection_Deserialization()
        {
            for (int i = 0; i < N; i++)
            {
                testDataCsv.DeserializeFromCsv<TestData>();
            }
        }

        [Benchmark]
        public void Json_Deserialization()
        {
            for (int i = 0; i < N; i++)
            {
                JsonSerializer.Deserialize<TestData>(testDataJson);
            }
        }
    }
}
