﻿using BenchmarkDotNet.Running;
using System;

namespace ParallelComputing.Benchmarks
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Benchmark started.");

            var summary = BenchmarkRunner.Run<ArraySumBenchmark>();

            Console.WriteLine($"Benchmark finished. Total elapsed time: {summary.TotalTime}");
            Console.WriteLine($"Results directory: {summary.ResultsDirectoryPath}");
        }
    }
}
