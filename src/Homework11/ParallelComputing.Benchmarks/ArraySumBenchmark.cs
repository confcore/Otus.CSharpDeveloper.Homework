﻿using BenchmarkDotNet.Attributes;
using ParallelComputing.Core;
using System;
using System.Linq;

namespace ParallelComputing.Benchmarks
{
    [ShortRunJob]
    public class ArraySumBenchmark
    {
        private const int MinArrayValue = 1;
        private const int MaxArrayValue = 50;

        private int[] array;

        [Params(100_000, 1_000_000, 10_000_000)]
        public int ArraySize;

        [GlobalSetup]
        public void Setup()
        {
            Random random = new Random();
            array = Enumerable
                .Repeat(0, ArraySize)
                .Select(i => random.Next(MinArrayValue, MaxArrayValue))
                .ToArray();
        }

        [Benchmark]
        public int SequentialSum() => array.SequentialSum();

        [Benchmark]
        public int ParallelSum() => array.ParallelSum();

        [Benchmark]
        public void ParallelLinqSum() => array.ParallelLinqSum();
    }
}
