﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ParallelComputing.Core
{
    public static class ArrayExtensions
    {
        public static int SequentialSum(this int[] source)
        {
            int sum = 0;

            for (int i = 0; i < source.Length; i++)
            {
                sum += source[i];
            }

            return sum;
        }

        public static int ParallelSum(this int[] source)
        {
            int sum = 0;
            var threads = new List<Thread>();
            int threadsCount = Environment.ProcessorCount;
            int chunkSize = (source.Length + threadsCount - 1) / threadsCount;

            for (var i = 0; i < (float)source.Length / chunkSize; i++)
            {
                var j = i;
                threads.Add(new Thread(() =>
                {
                    var subsum = source.Skip(j * chunkSize).Take(chunkSize).Sum();
                    Interlocked.Add(ref sum, subsum);
                }));
            }

            threads.ForEach(x => x.Start());
            threads.ForEach(x => x.Join());

            return sum;
        }

        public static int ParallelLinqSum(this int[] source) => source.AsParallel().Sum();
    }
}
