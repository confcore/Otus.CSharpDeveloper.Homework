﻿using Chatting.Extensions;
using Chatting.Messaging;
using System;
using System.Net.Sockets;
using System.Text;

namespace TcpChat.Client
{
    public class TcpChatClient : IDisposable
    {
        public event MessageReceivedHandler OnMessageReceived;
        public delegate void MessageReceivedHandler(string message);

        private readonly TcpClient client = new TcpClient();

        private volatile bool isRunning;
        private NetworkStream stream;

        public TcpChatClient(string userName)
        {
            UserName = userName;
        }

        public string UserName { get; private set; }

        public void Connect(string host, int port)
        {
            client.Connect(host, port);
            stream = client.GetStream();
            SendConnectMessage();
        }

        public void Send(string message)
        {
            var chatMessage = new Message
            {
                Text = message,
                User = UserName,
                Type = MessageType.Default
            };
            SendMessage(chatMessage);
        }

        public void Start()
        {
            isRunning = true;
            while (isRunning)
            {
                try
                {
                    var message = stream.WaitNextMessage();
                    OnMessageReceived?.Invoke(message.ToString());
                }
                catch
                {
                    Stop();
                    throw;
                }
            }
        }

        public void Stop()
        {
            isRunning = false;
            UnsubscribeAll();
            stream?.Close();
            client?.Close();
        }

        public void Dispose()
        {
            Stop();
        }

        private void UnsubscribeAll()
        {
            if (OnMessageReceived == null)
            {
                return;
            }

            Delegate[] subscribers = OnMessageReceived.GetInvocationList();
            foreach (var subscriber in subscribers)
            {
                OnMessageReceived -= subscriber as MessageReceivedHandler;
            }
        }

        private void SendMessage(Message jsonChatMessage)
        {
            byte[] data = Encoding.Unicode.GetBytes(jsonChatMessage.ToJson());
            stream.Write(data, 0, data.Length);
        }

        private void SendConnectMessage()
        {
            var connectMessage = new Message()
            {
                User = UserName,
                Type = MessageType.Connect
            };
            SendMessage(connectMessage);
        }
    }
}
