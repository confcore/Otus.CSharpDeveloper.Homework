﻿using Chatting;
using Chatting.Extensions;
using Chatting.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace TcpChat.Server
{
    public class TcpChatServer : IDisposable
    {
        public event MessageReceivedHandler OnMessageReceived;
        public delegate void MessageReceivedHandler(string message);

        private readonly IPAddress address;
        private readonly int port;
        private readonly List<TcpClient> clients = new List<TcpClient>();

        private volatile bool isRunning;
        private TcpListener tcpListener;

        public TcpChatServer(IPAddress address, int port)
        {
            this.address = address;
            this.port = port;
        }

        public void Start()
        {
            isRunning = true;
            try
            {
                tcpListener = new TcpListener(address, port);
                tcpListener.Start();

                while (isRunning)
                {
                    TcpClient tcpClient = tcpListener.AcceptTcpClient();
                    clients.Add(tcpClient);
                    ThreadPool.QueueUserWorkItem(ListenClient, tcpClient);
                }
            }
            catch
            {
                Stop();
                throw;
            }
        }

        public void Stop()
        {
            isRunning = false;
            tcpListener?.Stop();
            clients.Clear();
        }

        public void Dispose()
        {
            Stop();
        }

        private void ListenClient(object client)
        {
            var tcpClient = client as TcpClient;
            try
            {
                var stream = tcpClient.GetStream();
                var message = stream.WaitNextMessage();
                BroadcastMessage(message);
                OnMessageReceived?.Invoke(message.ToString());

                while (true)
                {
                    try
                    {
                        message = stream.WaitNextMessage();
                        OnMessageReceived?.Invoke(message.ToString());
                        BroadcastMessage(message);
                    }
                    catch
                    {
                        message.Type = MessageType.Disconnect;
                        OnMessageReceived?.Invoke(message.ToString());
                        BroadcastMessage(message);
                        break;
                    }
                }
            }
            finally
            {
                clients.Remove(tcpClient);
                tcpClient.Close();
            }
        }

        private void BroadcastMessage(Message message)
        {
            foreach (var client in clients.Where(c => c.Connected == true))
            {
                byte[] data = Encoding.Unicode.GetBytes(message.ToJson());
                client.GetStream().Write(data, 0, data.Length);
            }
        }
    }
}
