﻿using System.Text.Json;
using Chatting.Messaging;

namespace Chatting.Extensions
{
    public static class MessageExtensions
    {
        public static string ToJson(this Message message)
        {
            return JsonSerializer.Serialize(message);
        }

        public static Message FromJson(this string json)
        {
            return JsonSerializer.Deserialize<Message>(json);
        }
    }
}
