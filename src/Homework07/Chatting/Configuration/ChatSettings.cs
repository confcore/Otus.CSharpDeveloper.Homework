﻿namespace Chatting.Configuration
{
    public class ChatSettings
    {
        public string Address { get; set; }

        public int Port { get; set; }
    }
}
