﻿using System.Collections.Generic;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class AccountOperationsHistory
    {
        public Account Account { get; set; }

        public IEnumerable<OperationsHistory> History { get; set; }
    }
}
