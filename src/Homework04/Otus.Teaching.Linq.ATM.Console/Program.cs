﻿

namespace Otus.Teaching.Linq.ATM.Console
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Otus.Teaching.Linq.ATM.Core.Entities;
    using Otus.Teaching.Linq.ATM.Core.Services;
    using Otus.Teaching.Linq.ATM.DataAccess;

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            bool exit = false;
            while (!exit)
            {
                try
                {
                    ShowAllQueries();
                    Console.WriteLine("Введите номер запроса (от 1 до 5), для выхода введите 0:");
                    int queryNumber = int.Parse(Console.ReadLine());
                    switch (queryNumber)
                    {
                        case 0:
                            exit = true;
                            break;
                        case 1:
                            ShowUser(atmManager);
                            break;
                        case 2:
                            ShowAccounts(atmManager);
                            break;
                        case 3:
                            ShowAccountsWithHistory(atmManager);
                            break;
                        case 4:
                            ShowAllInputCashOperations(atmManager);
                            break;
                        case 5:
                            ShowUsersWithCashGreaterThan(atmManager);
                            break;
                        default:
                            Console.WriteLine("Некорректный ввод.");
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Ошибка: {ex}");
                }

                Console.WriteLine();
            }

            Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        private static void ShowAllQueries()
        {
            Console.WriteLine("Список доступных запросов:");
            Console.WriteLine("1. Вывод информации о заданном аккаунте по логину и паролю.");
            Console.WriteLine("2. Вывод данных о всех счетах заданного пользователя.");
            Console.WriteLine("3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту.");
            Console.WriteLine("4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта.");
            Console.WriteLine("5. Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой).");
        }

        /// <summary>
        /// 1. Вывод информации о заданном аккаунте по логину и паролю.
        /// </summary>
        /// <param name="atmManager">Банкомат.</param>
        private static void ShowUser(ATMManager atmManager)
        {
            User user = GetUserByConsole(atmManager);
            Console.WriteLine(user == null ? "Пользователь не найден." : user.ToString());
        }

        /// <summary>
        /// 2. Вывод данных о всех счетах заданного пользователя.
        /// </summary>
        /// <param name="atmManager">Банкомат.</param>
        private static void ShowAccounts(ATMManager atmManager)
        {
            User user = GetUserByConsole(atmManager);
            if (user == null)
            {
                Console.WriteLine("Пользователь не найден.");
                return;
            }

            var accounts = atmManager.GetAccounts(user);
            if (!accounts.Any())
            {
                Console.WriteLine($"У пользователя {user.Id} не найдено счетов.");
                return;
            }

            foreach (var account in accounts)
            {
                Console.WriteLine(account);
            }
        }

        /// <summary>
        /// 3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту.
        /// </summary>
        /// <param name="atmManager">Банкомат.</param>
        private static void ShowAccountsWithHistory(ATMManager atmManager)
        {
            User user = GetUserByConsole(atmManager);
            if (user == null)
            {
                Console.WriteLine("Пользователь не найден.");
                return;
            }

            var accountsWithHistory = atmManager.GetAccountsWithHistory(user);
            PrintAccountsWithHistory(accountsWithHistory);
        }

        /// <summary>
        /// 4. Вывод данных о всех операциях пополнения счёта.
        /// с указанием владельца каждого счёта.
        /// </summary>
        /// <param name="atmManager">Банкомат.</param>
        private static void ShowAllInputCashOperations(ATMManager atmManager)
        {
            var operations = atmManager.GetAllInputCashOperations();
            if (!operations.Any())
            {
                Console.WriteLine("Нет данных.");
                return;
            }

            foreach (var userOperations in operations)
            {
                Console.WriteLine($"Пользователь: {userOperations.User}");
                PrintAccountsWithHistory(userOperations.History);
                Console.WriteLine();
            } 
        }

        /// <summary>
        /// 5. Вывод данных о всех пользователях у которых на счёте 
        /// сумма больше N (N задаётся из вне и может быть любой).
        /// </summary>
        /// <param name="atmManager">Банкомат.</param>
        private static void ShowUsersWithCashGreaterThan(ATMManager atmManager)
        {
            Console.WriteLine("Введите N:");
            decimal cash = decimal.Parse(Console.ReadLine());

            var users = atmManager.GetUsersWithCashGreaterThan(cash);

            if (!users.Any())
            {
                Console.WriteLine("Нет данных.");
                return;
            }

            foreach (var user in users)
            {
                Console.WriteLine(user);
            }
        }

        private static User GetUserByConsole(ATMManager atmManager)
        {
            Console.WriteLine("Введите логин:");
            var login = Console.ReadLine();

            Console.WriteLine("Введите пароль:");
            var password = GetConsolePassword();

            return atmManager.GetUser(login, password);
        }

        private static void PrintAccountsWithHistory(IEnumerable<AccountOperationsHistory> accountsWithHistory)
        {
            if (!accountsWithHistory.Any())
            {
                Console.WriteLine($"Не найдено ни одного счета.");
                return;
            }

            foreach (var account in accountsWithHistory)
            {
                Console.WriteLine($"Счет: {account.Account}");
                if (account.History.Any())
                {
                    foreach (var history in account.History)
                    {
                        Console.WriteLine($"\t{history}");
                    }
                }
                else
                {
                    Console.WriteLine("\tНет операций по счету");
                }
            }
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }

        private static string GetConsolePassword()
        {
            StringBuilder result = new StringBuilder();
            while (true)
            {
                var consoleKey = Console.ReadKey(true);
                if (consoleKey.Key == ConsoleKey.Enter)
                {
                    Console.WriteLine();
                    break;
                }

                if (consoleKey.Key == ConsoleKey.Backspace)
                {
                    if (result.Length > 0)
                    {
                        Console.Write("\b\0\b");
                        result.Length--;
                    }

                    continue;
                }

                Console.Write('*');
                result.Append(consoleKey.KeyChar);
            }

            return result.ToString();
        }
    }
}